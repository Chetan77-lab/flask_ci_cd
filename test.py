from app import app
import unittest
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import json
from app import User,db


class BasicTest(unittest.TestCase):
    app = Flask(__name__)
    db=SQLAlchemy(app)
    def setUp(self):
        app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@localhost/test_flask_db'
        self.tester = app.test_client()
        db.create_all()
   


class Test_app(BasicTest):
    def test_get(self):
        res = self.tester.get("/getemployees")
        self.assertEqual(res.status_code,200)
        self.assertIn("email",str(res.data))


    def test_post(self):
        user = json.dumps({
             "name" : "test",
            "email" :"test@gmail.com"
                     })
        res = self.tester.post("/adduser",data=user
           
        ,content_type="application/json")
        self.assertEqual(res.status_code,200)
        

    def test_user_by_id(self):
        res = self.tester.get("/getuserbyid/17")
        self.assertEqual(res.status_code,200)
        self.assertIn("email",str(res.data))







if __name__ == "__main__":
    unittest.main()
    self.app.run()