from flask import Flask,request,jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import json
from flask_cors import CORS


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@localhost/flask_db'
db=SQLAlchemy(app)
ma= Marshmallow(app)
CORS(app)





class User(db.Model):
    id=db.Column(db.Integer,primary_key=True,autoincrement=True)
    name = db.Column(db.String(200),nullable=False)
    email = db.Column(db.String(200),nullable=False)

    def __repr__(self):
        return '<User %r' % self.id

@app.route("/getemployees",methods=['GET'])
def home():
    l = User.query.all()
    usr_sch = UserSchema(many=True)
    return jsonify(usr_sch.dump(l)),201

@app.route("/adduser",methods=['POST'])
def adduser():
    data=request.get_json()
    try:
        u = User(name=data['name'],email=data['email'])
        db.session.add(u)
        db.session.commit()
        return "Inserted",201
    except:
        return "Failed",404

@app.route("/getuserbyid/<id>")
def getuserbyid(id):
    u = User.query.get(id)
    usr_ser = UserSchema()
    return usr_ser.dump(u)
    
@app.route("/deleteuser/<id>",methods=['DELETE'])
def deleteuser(id):
    try:
        u=User.query.get(id)
        db.session.delete(u)
        db.session.commit()
        return "Deleted"
    except:
        return "Failed to Delete"

@app.route("/edituser",methods=['PUT'])
def edituser():
    try:
        data = request.get_json()
        u = User.query.get(data['id'])
        u.name = data['name']
        u.email = data['email']
        print(u)
        db.session.add(u)
        db.session.commit()
        return "Updated"
    except:
        return "Failed to Update"

    





class UserSchema(ma.Schema):
    class Meta:
        fields =("id","name","email")
        model = User


if __name__ == "__main__":
    app.run(debug=True)