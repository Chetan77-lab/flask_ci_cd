import unittest 
from CalculatorApp import CalculatorApp

class TestArithmetic(unittest.TestCase):
    def test_add(self):
        calc = CalculatorApp()
        self.assertEqual(4,calc.add(2,2))


    def test_diff(self):
        calc = CalculatorApp()
        self.assertEqual(-2,calc.substract(1,3))


    def test_div(self):
        calc = CalculatorApp()
        self.assertEqual(2,calc.division(4,2))
        self.assertEqual(10,calc.division(100,10))


    def test_multiply(self):
        calc = CalculatorApp()
        self.assertEqual(4,calc.multiply(2,2))



    



if __name__ == "__main__":
    unittest.main()