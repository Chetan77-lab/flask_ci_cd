from app import app,db
import unittest
from flask_sqlalchemy import SQLAlchemy
from flask import Flask,jsonify
import json


class BasicTest(unittest.TestCase):
    
    db=SQLAlchemy(app)
    def setUp(self):
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        self.tester = app.test_client()
        db.create_all()
   


class Test_app(BasicTest):
  


    def test_post(self):
        user = json.dumps({
             "name" : "test2",
            "email" :"test@gmail.com"
                     })
        res = self.tester.post("/adduser",data=user
           
        ,content_type="application/json")
        self.assertEqual(res.status_code,201)

    def test_get(self):
        res = self.tester.get("/getemployees")
        self.assertEqual(res.status_code,201)
        
        

    def test_user_by_id(self):
        res = self.tester.get("/getuserbyid/1")
        self.assertEqual(res.status_code,200)   
        self.assertIn("email",str(res.data))


    def test_user_by_wrngid(self):
        res = self.tester.get("/getuserbyid/")
        self.assertEqual(res.status_code,404)
        
        








if __name__ == "__main__":
    unittest.main()
    self.app.run()
